import sys
sys.path.append('../')

from omegaconf import DictConfig
import hydra
import os
import pickle
from tqdm import tqdm
from sklearn.model_selection import train_test_split

import mlflow

from transformers BertTokenizer

from utils.dataset import BERTJapaneseDataset
from utils.model import BERTJapaneseModel
from utils.trainer import Trainer

def seed_everything(seed=42):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)

def freeze_weights(model_bert):
    for _, param in model_bert.named_parameters():
        param.requires_grad = False

    for _, param in model_bert.bert.encoder.layer.named_parameters():
        param.requires_grad = True

    for _, param in model_bert.fc.named_parameters():
        param.requires_grad = True

@hydra.main(config_path='../config', config_name='config')
def main(cfg: DictConfig):
    # 動作ディレクトリ移動
    orig_dir = hydra.utils.get_original_cwd()
    os.chdir(orig_dir)
    print(f'Original working directry: {orig_dir}')

    # mlflowによるtracking開始
    mlflow.set_tracking_uri(cfg.path.mlruns)
    mlflow.set_experiment(cfg.experiment_name)
    mlflow.start_run()
    mlflow.log_params('epochs', cfg.training.epochs)
    mlflow.log_params('batch_size', cfg.training.batch_size)
    mlflow.log_params('learning_rate', cfg.training.lr)

    # seed値固定
    seed_everything(42)

    # DataFrameをロードしてtrainとvalidに分割
    df_train = pd.read_csv(cfg.path.train)
    df_train, df_valid = train_test_split(df_train, test_size=0.3)

    # 学習に必要なmodel, optimizerなどを宣言
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu' 
    trainer = Trainer(device)
    model = BERTJapaneseModel(cfg.pretrained)
    freeze_weights(model)# bertのencoder layerとその下のfc層以外のパラメータを凍結
    optimizer = torch.optim.Adam(model.parameters, lr=cfg.training.lr)
    criterion = torch.nn.BCEWithLogitsLoss()

    # tokenizerはpretrainedに合わせたものをロードする
    tokenizer = BertJapaneseTokenizer.from_pretrained(cfg.pretrained)

    # dataloaderを作成
    dataset_train = BERTJapaneseDataset(df_train, tokenizer)
    dataloader_valid = torch.utils.data.DataLoader(dataset_train,
                                                   batch_size=cfg.training.batch_size,
                                                   shuffle=True,
                                                   pin_memory=True,
                                                   num_workers=4)

    dataset_valid = BERTJapaneseDataset(df_valid, tokenizer)
    dataloader_valid = torch.utils.data.DataLoader(dataset_valid,
                                                   batch_size=cfg.training.batch_size,
                                                   shuffle=True,
                                                   pin_memory=True,
                                                   num_workers=4)
    
    best_loss = 1e10
    for epoch in range(cfg.training.epochs):
        print(f'epoch {epoch+1} started...')
        train_loss = trainer.train_one_epoch(model, optimizer, criterion, dataloader_train)
        valid_loss = trainer.valid_one_epoch(model, optimizer, criterion, dataloader_valid)
        
        print(f'train_loss: {train_loss:.5f}, valid_loss: {valid_loss:.5f}')
        if valid_loss<best_loss:
            # valid lossが最も小さくなるモデルのcheckpointを保存する
            print('saving weights at best valid loss...')
            torch.save(model.state_dict(), os.path.join(cfg.save_path, 'bert_model.pth'))
            best_loss = valid_loss

        # train, validそれぞれのlossを記録する
        mlflow.log_metric(f'train_loss', train_loss)
        mlflow.log_metric(f'valid_loss', valid_loss)
    mlflow.end_run()

if __name__=='__main__':
    main()