import torch
from tqdm import tqdm
import os
import mlflow

class Trainer:
    def __init__(self, device):
        self.device = device

    def train_one_epoch(self, model, optimizer, criterion, dataloader):
        train_loss = 0
        steps = 0
        model.train()
        prog_bar = tqdm(dataloader)
        for batch in prog_bar:
            steps+=1

            optimizer.zero_grad()

            ids = batch['ids'].to(self.device)
            mask = batch['mask'].to(self.device)
            label = batch['y'].to(self.device)

            logits = model(ids, mask)

            loss = criterion(logits, label)
            loss.backward()
            optimizer.step()

            train_loss += loss.item()
            prog_bar.set_description(f'train loss: {train_loss/steps:.5f}')
        return train_loss/len(dataloader)
    
    def valid_one_epoch(self, model, optimizer, criterion, dataloader):
        valid_loss = 0
        steps = 0
        model.eval()
        prog_bar = tqdm(dataloader)
        for batch in prog_bar:
            steps += 1

            ids = batch['ids'].to(self.device)
            mask = batch['mask'].to(self.device)
            label = batch['y'].to(self.device)

            with torch.no_grad():
                logits = model(ids, mask)
                loss = criterion(logits, label)
                valid_loss += loss.item()
            
            prog_bar.set_description(f'valid loss: {valid_loss/steps:.5f}')
        return valid_loss/len(dataloader)
