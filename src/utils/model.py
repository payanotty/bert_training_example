from transformers import BertModel
import torch

class BERTJapaneseModel(torch.nn.Module):
    def __init__(self, pretrained='cl-tohoku/bert-japanese-whole-word-masking'):
        super().__init__()
        self.bert = BertModel.from_pretrained(pretrained)
        self.dropout = torch.nn.Dropout(0.1)
        self.fc = torch.nn.Linear(768, 1)
    
    def forward(self, ids, mask):
        out = self.bert(ids, mask)
        out = out['pooler_output']
        out = self.dropout(out)
        out = self.fc(out)
        return out

