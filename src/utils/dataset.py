import torch
import numpy as np

class BERTJapaneseDataset(torch.utils.data.Dataset):
    def __init__(self, df, tokenizer, max_length=128):
        self.df = df
        self.tokenizer = tokenizer
        self.max_length = max_length
    
    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        text = self.df.text[idx]
        inputs = self.tokenizer.encode_plus(text,
                                            add_special_tokens=True,
                                            max_length=self.max_length,
                                            truncation=True,
                                            padding='max_length')
        ids = torch.LongTensor(inputs['input_ids'])
        mask = torch.LongTensor(inputs['attention_mask'])
        label = torch.LongTensor(self.df.label[idx])
        return {'ids': ids, 'mask': mask, 'y': label}